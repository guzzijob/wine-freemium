from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_script import Manager
from app.config import Config
from flask_migrate import Migrate, MigrateCommand
from werkzeug.security import check_password_hash, generate_password_hash
import argparse
#setup needed objects
app = Flask(__name__)
app.config.from_object(Config)
db = SQLAlchemy(app)

from app.models import *

USER_GROUPS= {
    "administrator":"ADMIN",
    "supplier":"SUP",
    "distributor":"DIST"
}


if __name__=="__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--admin_password",help="admin user default password",required=True)
    parser.add_argument("--admin_username",help="admin username",required=True)
    parser.add_argument("--admin_email",help="admin username",required=True)
    args = parser.parse_args()
    for group_name in USER_GROUPS.keys():
        group = user_groups()
        group.group_desc = group_name
        group.role = USER_GROUPS[group_name]
        db.session.add(group)

    db.session.commit()
    #initial admin user
    groups_admin = db.session.query(user_groups).filter_by(group_desc="administrator").first()

    user = users()
    user.email = args.admin_email
    user.username = args.admin_username
    user.sup_dist_admin = groups_admin.id
    hashed_password = generate_password_hash(args.admin_password,method='sha256') #80 chars
    user.password=hashed_password

    db.session.add(user)
    db.session.commit()


