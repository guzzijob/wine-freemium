# Import Form and RecaptchaField (optional)
from flask_wtf import FlaskForm # , RecaptchaField

# Import Form elements such as TextField and BooleanField (optional)
from wtforms import SelectField, StringField, PasswordField , BooleanField, SubmitField
from wtforms.validators import DataRequired,Length


# Import Form validators
from wtforms.validators import Required, Email, EqualTo

from app.constants import SUP,DIST,ADMIN
# Define the login form (WTForms)
class SignupForm(FlaskForm):
    user = StringField('Username',validators=[DataRequired()])
    email = StringField('Email Address',validators=[DataRequired(),Email()])
    password = PasswordField('Password',validators=[DataRequired(),Length(min=8,max=80)])
    #sup_dist_choices = [(SUP,'Supplier'),(DIST,'Distributor'),(ADMIN,'Administrator')]
    #sup_dist = SelectField('User Type',choices=sup_dist_choices)
    submit = SubmitField('Sign In')

class LoginForm(FlaskForm):
    user = StringField('Username',validators=[DataRequired()])
    password = PasswordField('Password',validators=[DataRequired(),Length(min=8,max=80)])
    remember_me = BooleanField('Remember Me')
    submit = SubmitField('Sign In')
