# Import flask dependencies
from flask import Blueprint, request, render_template, \
                  flash,redirect,url_for

# Import password / encryption helper tools
from werkzeug.security import check_password_hash, generate_password_hash
from flask_login import login_user,login_required,logout_user,current_user
# Import the database object from the main app module

# Import module forms
from .forms import LoginForm,SignupForm

# Import module models (i.e. User)
from app import db
from app.models import users

# Define the blueprint: 'auth', set its url prefix: app.url/auth
auth = Blueprint('auth', __name__, url_prefix='/auth',template_folder='templates')

def flash_errors(form):
    for field, errors in form.errors.items():
        for error in errors:
            flash(u"Error in the %s field - %s" % (
                getattr(form, field).label.text,
                error
            ))

@auth.route('/signup',methods=['GET','POST'])
def signup():
    # If sign in form is submitted
    form = SignupForm(request.form)
    if request.method=="POST":
        if form.validate_on_submit():
            hashed_password = generate_password_hash(form.password.data,method='sha256') #80 chars
            new_user = users(username=form.user.data,email=form.email.data,
                             password=hashed_password)
            db.session.add(new_user)
            db.session.commit()
            flash("new user created",'message')
            redirect(url_for('auth.signin'))
        else:
            flash_errors(request.form)

    return render_template("auth/signup.html", form=form)


@auth.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('index'))

# Set the route and accepted methods
@auth.route('/signin', methods=['GET', 'POST'])
def signin():

    # If sign in form is submitted
    form = LoginForm(request.form)
    import app.models as models
    # Verify the sign in form
    if form.validate_on_submit():

        from app.constants import SUP,DIST,ADMIN
        from app.models import user_groups
        user = db.session.query(users).filter_by(username=form.user.data).first()
        if user:

            if check_password_hash(user.password,form.password.data):
                #flash('Welcome %s' % user.username)
                login_user(user,remember=form.remember_me.data)

                if user.group.role== SUP:
                    return redirect(url_for('dashboard'))
                if user.group.role== DIST:
                    return redirect(url_for('dashboard'))
                if user.group.role== ADMIN:
                    return redirect(url_for('administrator.dashboard'))
            else:
                flash('Password not valid',"error")
                return redirect(url_for('auth.signin'))
        else:
            flash('Username not valid',"error")
            return redirect(url_for('auth.signin',supplier=user.sup, user_fields=user))

    return render_template("auth/signin.html", form=form)

