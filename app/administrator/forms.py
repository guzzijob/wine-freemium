# Import Form and RecaptchaField (optional)
from flask_wtf import FlaskForm # , RecaptchaField

# Import Form elements such as TextField and BooleanField (optional)
from wtforms import SelectField, StringField, PasswordField , BooleanField, SubmitField
from wtforms.validators import DataRequired,Length
from wtforms_alchemy import ModelForm,ModelFieldList,QuerySelectField
from wtforms import FormField
from flask import url_for
from flask_table import Table, Col,LinkCol,ButtonCol
# Import Form validators
from wtforms.validators import Required, Email, EqualTo

from app import db
from app.models import users,user_groups,sup_master,dist_master,states

def get_parks():
    return db.session.query(states).all()

class SupForm(ModelForm):
    class Meta:
        model=sup_master
        #include=['master_sup_id']
    state = QuerySelectField(get_label='state',query_factory=get_parks)


class GroupFormEdit(ModelForm):
    class Meta:
        model=user_groups


class StateForm(ModelForm):
    class Meta:
        model=states


class EntityForm(FlaskForm):
    entity = SelectField('Entity')
    submit = SubmitField('Submit')


def groups_choices():
    results = db.session.query(user_groups).all()
    return results


class GroupForm(FlaskForm):
    #todo(aj) move this into the form code
    #results = db.session.query(user_groups).all()
    #choices = [(record.id,record.group_desc) for record in results]

    group = QuerySelectField("Group",query_factory = groups_choices,get_label='group_desc')
    submit = SubmitField('Submit')


class UserTable(Table):
    user_id=Col('ID')
    username=Col('Username')
    group_desc=Col('User Type')
    sup_id=Col('Supplier ID')
    supplier_name = Col('Supplier Name')
    dist_id=Col('Distributor ID')
    dist_name = Col('Distributor Name')
    edit = LinkCol('Edit', 'administrator.edit_user', url_kwargs=dict(user_id='user_id'))

class SortTable(Table):
    def __init__(self,items,endpoint=None,**kwargs):
        super(SortTable,self).__init__(items,**kwargs)
        self.endpoint=endpoint

    allow_sort = True
    def sort_url(self,col_key=None,reverse=False):
        if reverse:
            direction = 'desc'
        else:
            direction = 'asc'

        return url_for('.%s'%self.endpoint,
                       sort=col_key,
                       direction=direction)


class StateTable(SortTable):
    state = Col('State')
    #blueprint = 'administrator'
    url_endpoint = "state"
    def __init__(self,items,endpoint=None,**kwargs):
        super(StateTable,self).__init__(items,endpoint=endpoint,**kwargs)
    #    self.endpoint=endpoint
    """delete = LinkCol('remove',blueprint+'.'+ url_endpoint,
                     url_kwargs=dict(obj_id='id'),
                     anchor_attrs={'class':'text-danger'},
                     url_kwargs_extra=dict(operation='delete'),
                     allow_sort=False)
    edit = LinkCol('Edit',blueprint+'.'+ url_endpoint,
                   url_kwargs=dict(obj_id='id'),
                   url_kwargs_extra=dict(operation='edit'),
                   allow_sort=False)
    """
    """
    #todo(aj) this can be fixed by passing the blueprint and endpoint into the constructor
    allow_sort = True
    def sort_url(self,col_key=None,reverse=False):
        if reverse:
            direction = 'desc'
        else:
            direction = 'asc'

        return url_for('.%s'%self.endpoint,
                       sort=col_key,
                       direction=direction)

    """


class GroupTable(SortTable):
    url_endpoint='groups'
    #blueprint='administrator'
    group_desc = Col('Group Description')
    role = Col('Role')

    def __init__(self,items,endpoint=None,**kwargs):
        super(GroupTable,self).__init__(items,endpoint=endpoint,**kwargs)
    """delete = LinkCol('remove',blueprint+'.'+ url_endpoint,
                     url_kwargs=dict(obj_id='id'),
                     anchor_attrs={'class':'text-danger'},
                     url_kwargs_extra=dict(operation='delete'),
                     allow_sort=False)
    edit = LinkCol('Edit',blueprint+'.'+ url_endpoint,
                   url_kwargs=dict(obj_id='id'),
                   url_kwargs_extra=dict(operation='edit'),
                   allow_sort=False)
    allow_sort = True
    def sort_url(self,col_key=None,reverse=False):
        if reverse:
            direction = 'desc'
        else:
            direction = 'asc'

        return url_for(StateTable.blueprint+'.'+StateTable.url_endpoint,
                       sort=col_key,
                       direction=direction)
    """

class SupTable(SortTable):
    url_endpoint='sup_master'

    master_sup_id= Col('ID')
    supplier_name=Col('Supplier Name')
    state=Col('State')
    supplier_zip=Col('Zip')
    def __init__(self,items,endpoint=None,**kwargs):
        super(SupTable,self).__init__(items,endpoint=endpoint,**kwargs)

    """
    delete = LinkCol('remove','administrator.'+ url_endpoint,
                     url_kwargs=dict(obj_id='master_sup_id'),
                     anchor_attrs={'class':'text-danger'},
                     url_kwargs_extra=dict(operation='delete'),
                     allow_sort=False)
    edit = LinkCol('Edit','administrator.'+ url_endpoint,
                   url_kwargs=dict(obj_id='master_sup_id'),
                   url_kwargs_extra=dict(operation='edit'),
                   allow_sort=False)
    allow_sort = True

    # todo(aj) override constructor. pass in endpoint for this method
    # todo(aj) add instance variable for endpoint to be set in constructor for this method
    # then successfully abstracted all the duplicate code
    def sort_url(self,col_key=None,reverse=False):
        if reverse:
            direction = 'desc'
        else:
            direction = 'asc'

        return url_for('administrator.'+SupTable.url_endpoint,
                       sort=col_key,
                       direction=direction)
    """

