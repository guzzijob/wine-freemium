# Import flask dependencies
from flask import Blueprint, request, render_template, \
                  flash,redirect,url_for

# Import password / encryption helper tools
from werkzeug.security import check_password_hash, generate_password_hash
from flask_login import login_user,login_required,logout_user,current_user
# Import the database object from the main app module
from flask_admin.contrib.sqla import ModelView
from .forms import GroupForm,EntityForm
# Import module models (i.e. User)
from app import db
from app.models import users,sup_master
from .forms import SupTable,GroupTable,StateForm,StateTable
from app.models import sup_master,user_groups,states
from .forms import SupForm, GroupFormEdit
from flask.views import MethodView

from wtforms.ext.sqlalchemy.orm import model_form
# Define the blueprint: 'auth', set its url prefix: app.url/auth
administrator = Blueprint('administrator', __name__, url_prefix='/administrator',template_folder='templates')
"""
class LoginView(ModelView):

    def is_accessible(self):
        return current_user.is_authenticated

    def inaccessible_callback(self, name, **kwargs):
        # redirect to login page if user doesn't have access
        return redirect(url_for('login', next=request.url))
login=LoginView(sup_master,db_session)

admin.add_view(login)
"""

def test(self):
    return True

class CrudView(MethodView):
    list_template='administrator/listview_table.html'
    detail_template='administrator/detailview.html'

    def __init__(self,model, endpoint, table,model_form=None,list_template=None,detail_template=None,exclude=None,filters=None):
        self.model=model
        self.table = table
        self.endpoint=endpoint
        self.path=url_for('.%s'%self.endpoint)
        if list_template:
            self.list_template=list_template
        if detail_template:
            self.detail_template=detail_template

        self.filters = filters or {}
        self.obj_form = model_form
        pass

    def render_detail(self,**kwargs):
        return render_template(self.detail_template,path=self.path,**kwargs)

    def render_list(self,**kwargs):
        return render_template(self.list_template,path=self.path,**kwargs)

    def get(self):
        #todo(aj) add filter operation
        # retrieve filtered object using header search patterns


        operation = request.args.get("operation")
        obj_id= request.args.get("obj_id")
        filter= request.args.get("filter")
        if operation=='new':
            form = self.obj_form(request.form)
            action = self.path
            return self.render_detail(form=form,action=action)
        if operation=='delete':
            result = db.session.query(self.model).get(obj_id)
            db.session.delete(result)
            db.session.commit()
            db.session.rollback()
            return redirect(self.path)

        #list view with filter
        if operation=='filter':
            func = self.filters.get(filter_name)
            result = func(self.model)
            return self.render_list(obj=result,filter_name=filter_name)

        if obj_id:
            obj = db.session.query(self.model).get(obj_id)
            form = self.obj_form(obj=obj)
            #action is the url that we will later use
            # to do the post, the same url with the obj_id in this case
            action = request.path+obj_id
            return self.render_detail(form=form,action=action)

        #todo(aj) add sort operation.
        sort = request.args.get('sort')
        obj=None
        reverse=None
        if sort is not None:
            reverse = (request.args.get('direction','asc') == 'desc')
            if reverse:
                order_by_col = getattr(self.model,sort).desc()
            else:
                order_by_col = getattr(self.model,sort)
            obj = db.session.query(self.model).order_by(order_by_col).all()
        else:
            obj = db.session.query(self.model).all()
        table_inst = self.table(obj,classes=['table'],sort_by=sort,sort_reverse=reverse,endpoint=self.endpoint)
        return self.render_list(obj=obj,table=table_inst)

    def post(self,obj_id = ''):
        #todo add checkboxes to allow operations like delete on multiple rows
        #either load an object to update if obj_id is given else
        # initiate a new object
        obj=None
        if obj_id:
            obj = db.session.query(self.model).get(obj_id)
        else:
            obj = self.model()
        form = self.obj_form(request.form)
        # this populates the obj from the form that we have in the request
        form.populate_obj(obj)
        db.session.add(obj)
        db.session.commit()
        return redirect(self.path)

    @staticmethod
    def register_crud(app, url, table, model, decorators=None, model_form=None, id_col=None,**kwargs):
        #todo(aj) just adding these isn't working.  also need to
        #todo(aj).. append to table._cols the ordered dict so they show up as well
        if type(app) == Blueprint:
            url_endpoint = app.name + '.' + table.url_endpoint
        else:
            url_endpoint=table.url_endpoint
        from flask_table import LinkCol

        table.delete = LinkCol('remove',url_endpoint,
                 url_kwargs=dict(obj_id=id_col),
                 anchor_attrs={'class':'text-danger'},
                 url_kwargs_extra=dict(operation='delete'),
                 allow_sort=False)
        table._cols['delete']=table.delete
        table.edit = LinkCol('Edit',url_endpoint,
                       url_kwargs=dict(obj_id=id_col),
                       url_kwargs_extra=dict(operation='edit'),
                       allow_sort=False)
        table._cols['edit']=table.edit

        view = CrudView.as_view(model_form=model_form,table=table,
                                endpoint=table.url_endpoint,name=table.url_endpoint, model=model,**kwargs)

        if decorators is not None:
            for decorator in decorators:
                view = decorator(view)
        #todo(aj) set url_endpoint for table based on 'blueprintendpoint.endpoint'
        #todo(aj) set id col for table based on primary key col from model
        #table.id_col = id_col
        #app.add_url_rule('%s/' % url, view_func=view,methods=['POST'])
        app.add_url_rule('%s/<string:obj_id>' % url, view_func=view,methods=['POST'])
        app.add_url_rule('%s/' %url,view_func=view)

    #    app.add_url_rule('%s/<operation>/<string:obj_id>/'%url,
    #                     view_func=view,
    #                     methods=['GET'])
    #    app.add_url_rule('%s/<operation>/<filter_name>/' %url, view_func=view,
    #                     methods=['GET'])


CrudView.register_crud(app=administrator,
              url='/suppliers',
              decorators=[login_required],
              #endpoint='sup_master',
              model_form=SupForm,
              table=SupTable,
              id_col="id",
              model=sup_master)

CrudView.register_crud(app=administrator,
              url='/groups',
              decorators=[login_required],
              #endpoint='groups',
              model_form=GroupFormEdit,
              table=GroupTable,
              id_col='id',
              model=user_groups)

CrudView.register_crud(app=administrator,
              url='/states',
              decorators=[login_required],
              #endpoint='states',
              model_form=StateForm,
              id_col='id',
              table=StateTable,
              model=states)

@administrator.route('/users')
@login_required
def users():
    from .forms import UserTable
    from app.models import users,user_groups,sup_master,dist_master
    users_list = db.session.query(users.username,
                                  users.user_id,
                                  users.sup_id,
                                  users.dist_id,
                                  user_groups.group_desc,
                                  sup_master.supplier_name,
                                  dist_master.dist_name,
                                  users.email).outerjoin(
                                    user_groups,
                                    users.sup_dist_admin==user_groups.id).outerjoin(
                                        sup_master,users.sup_id==sup_master.id).outerjoin(
                                        dist_master,users.dist_id==dist_master.id).all()
    table = UserTable(users_list,classes=['table'])
    return render_template('administrator/users.html',table=table)

@login_required
@administrator.route('/dashboard')
def dashboard():
    return render_template('administrator/dashboard.html',name=current_user.username)

@administrator.route('/edit_user/<int:user_id>',methods=["Get","Post"])
@login_required
def edit_user(user_id):
    from app.models import users
    user = db.session.query(users).filter(users.user_id==user_id).first()
    form=None
    if request.method=="POST":
        form = GroupForm(form=request.form)
    if request.method=="GET":
        form = GroupForm(obj=user)
    if user:
        if form.validate_on_submit():
            user.sup_dist_admin=form.group.data.id
            db.session.commit()
            flash("user updated")
            return redirect(url_for('administrator.settings',user_id=user_id))

        return render_template('administrator/edit_user.html',form=form)
    else:
        render_template('404.html')


@administrator.route('/settings/<int:user_id>',methods=['GET',"POST"])
@login_required
def settings(user_id):
    from load_user_types import USER_GROUPS
    from app.constants import SUP,DIST,ADMIN
    from app.models import sup_master,dist_master

    from app.models import users

    user = db.session.query(users).filter(users.user_id==user_id).first()

    #if type is s then sup_master else dist_master
    choices = None
    results = None
    form = None
    if request.method=="POST":
        form = EntityForm(obj=user)
    if request.method=="GET":
        form = EntityForm(request.form)
    #currentuser is instance of user
    if USER_GROUPS[user.group.group_desc]==SUP:
        results = db.session.query(sup_master).all()
        choices = [(str(record.id),record.supplier_name) for record in results]
        form.entity.choices=choices

    elif USER_GROUPS[user.group]==DIST:
        results = db.session.query(dist_master).all()
        choices = [(str(record.id),record.dist_name) for record in results]
        form.entity.choices=choices

    elif USER_GROUPS[user.group]==ADMIN:
        # skip this step and go to the dashboard
        return redirect(url_for('administrator.users'))
    else :
        # what are you
        render_template('404.html')

    # done setup all choices before validation.  else fail validation
    # form submitted
    if form.validate_on_submit():
        if USER_GROUPS[user.group.group_desc]==SUP:
            user.sup_id=form.entity.data
            db.session.commit()
        if USER_GROUPS[user.group.group_desc]==DIST:
            user.dist_id=form.entity.data
            db.session.commit()
        return redirect(url_for('administrator.users'))



    return render_template("administrator/settings.html",form=form)

