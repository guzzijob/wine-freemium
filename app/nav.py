from flask_nav import Nav
from flask_nav.elements import *

nav = Nav()
class NavContainer(object):
    home_view = View('Home','index')
    login_view=View('Login','auth.signin')

    logout_view=View('Logout','auth.logout')
    signup_view=View('Signup','auth.signup')

    topbar = Navbar('',
                    home_view,
                    login_view,
                    logout_view,
                    signup_view)
    sup = Subgroup('Supplier',
             View('Supplier Home','supplier.home'),
             View('Supplier Distributors','supplier.distributors'),
            View('Supplier Products','supplier.products'))

    admin =Subgroup(
        'Administration',
        View('Suppliers','administrator.sup_master'),
        View('States','administrator.state'),
        View('Groups','administrator.groups'),
        View('Users','administrator.users')
    )

    supbar = Navbar('', home_view,
                    logout_view,
                    sup)

    adminbar = Navbar('', home_view,
                    logout_view,
                    admin)

nav.register_element('top',NavContainer.topbar)
nav.register_element('sup',NavContainer.supbar)
nav.register_element('admin',NavContainer.adminbar)
