import os
def get_env_variable(name):
    try:
        return os.environ[name]
    except KeyError:
        message = "Expected environment variable '{}' not set.".format(name)
        raise Exception(message)

# the values of those depend on your setup

POSTGRES_USER = get_env_variable("POSTGRES_USER")
POSTGRES_PW= get_env_variable("POSTGRES_PW")

class Config(object):
    SECRET_KEY = 'somethingsecret'
    # Secret key for signing cookies
    SQLALCHEMY_DATABASE_URI = \
        "postgresql+psycopg2://{user}:{pw}@localhost:5432/crossmatch_v2".format(user=POSTGRES_USER,pw=POSTGRES_PW)
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    # Statement for enabling the development environment
    DEBUG = True

    # Define the application directory
    import os
    BASE_DIR = os.path.abspath(os.path.dirname(__file__))


    # Application threads. A common general assumption is
    # using 2 per available processor cores - to handle
    # incoming requests using one and performing background
    # operations using the other.
    THREADS_PER_PAGE = 2

    # Enable protection agains *Cross-site Request Forgery (CSRF)*
    CSRF_ENABLED     = True

    # Use a secure, unique and absolutely secret key for
    # signing the data.
    CSRF_SESSION_KEY = "secret"

