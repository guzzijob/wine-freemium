# Import flask dependencies
from flask import Blueprint, request, render_template, \
                  flash,redirect,url_for

# Import password / encryption helper tools
from werkzeug.security import check_password_hash, generate_password_hash
from flask_login import login_user,login_required,logout_user
# Import the database object from the main app module

# Import module forms

# Import module models (i.e. User)

# Define the blueprint: 'auth', set its url prefix: app.url/auth
supplier = Blueprint('supplier', __name__, url_prefix='/supplier',template_folder='templates')


@supplier.route('/<string:supplier>')
@login_required
def supplier_main(supplier):
    supplier=supplier
    render_template('supplier/main.html',supplier=supplier)


#todo(aj) add edit products

#todo(aj) add edit distributors
