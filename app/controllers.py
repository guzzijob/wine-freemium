from app import app
from sqlalchemy.exc import InvalidRequestError
from flask import Blueprint, request, render_template, \
    flash, redirect, url_for,jsonify
from flask_login import login_required,current_user

@app.route('/index')
@app.route('/')
def index():
    return render_template('index.html')

@app.route('/dashboard')
@login_required
def dashboard():
    return render_template('dummydashboard.html',name=current_user.username)

@app.route('/api/help', methods = ['GET'])
@login_required
def help():
    """Print available functions."""
    func_list = {}
    for rule in app.url_map.iter_rules():
        if rule.endpoint != 'static':
            func_list[rule.rule] = app.view_functions[rule.endpoint].__doc__
    return jsonify(func_list)