# Import flask and template operators
from flask import Flask, render_template
from flask_bootstrap import Bootstrap
from sqlalchemy.exc import InvalidRequestError
from sqlalchemy.exc import IntegrityError
import app.nav
from flask_login import LoginManager, UserMixin, login_user, login_required, logout_user, current_user
from flask_admin import Admin
from flask_principal import Principal,Permission,RoleNeed
from flask_sqlalchemy import SQLAlchemy
from app.config import Config
from flask_migrate import Migrate
# Define the WSGI application object
app = Flask(__name__)
app.config.from_object(Config)
Bootstrap(app)
db = SQLAlchemy(app)
#migrate = Migrate(app,db)

# Configurations
nav.nav.init_app(app)
admin = Admin(app)

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'auth.signin'

from app.models import users
principals = Principal(app)
"""identity_loaded.connect_via(app)
def on_identity_loaded(sender, identity):
    # Set the identity user object
    current_user = session.get('user', False)
    if not current_user:
        return False
    identity.user = current_user

    # Add the UserNeed to the identity
    if hasattr(current_user, 'id'):
        identity.provides.add(UserNeed(current_user.id))

    # Assuming the User model has a list of groups, update the
    # identity with the groups that the user provides
    if hasattr(current_user, 'groups'):
        groups = user.Group.query.filter(user.Group.users.any(id=current_user.id)).all()
        for group in groups:
            identity.provides.add(RoleNeed(group.name))
"""
# Sample HTTP error handling
@app.errorhandler(404)
def not_found(error):
    return render_template('404.html',error='404'), 404



@login_manager.user_loader
def load_user(user_id):
    user = db.session.query(users).filter(users.user_id == int(user_id)).first()
    if user:
        return user
    else:
        return None

@app.errorhandler(IntegrityError)
@app.errorhandler(InvalidRequestError)
def special_exception_handler(error):
    db.session.rollback()
    return 'Database connection failed' + str(error), 500

# Import a module / component using its blueprint handler variable (mod_auth)
from app.auth.controllers import auth as auth
from app.administrator.controllers import administrator as administrator
from app.supplier.controllers import supplier as supplier
from app import controllers
# Register blueprint(s)
app.register_blueprint(auth)
app.register_blueprint(supplier)
app.register_blueprint(administrator)
# app.register_blueprint(xyz_module)
