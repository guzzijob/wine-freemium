from app import db
from sqlalchemy import Column,String,Integer,ForeignKey

class dist_master(db.Model):
    __tablename__="dist_master"
    id=Column(Integer,primary_key=True,autoincrement=True)
    master_dist_id = Column(String(length=30),unique=True)
    dist_name= Column(String(length=30))
    # this changed so we may need to debug the drop down for the supplier form
    dist_state = Column(Integer,ForeignKey('states.id'))
    dist_zip = Column(Integer)
    state= db.relationship('states', foreign_keys=[dist_state])


class states(db.Model):
    __tablename__="states"
    id=Column(Integer,primary_key=True,autoincrement=True)
    state=Column(String(length=3))

    def __str__(self):
        return self.state


class sup_master(db.Model):
    __tablename__="sup_master"
    id=Column(Integer,primary_key=True,autoincrement=True)
    master_sup_id = Column(String(length=30),unique=True)
    supplier_name = Column(String(length=30))
    # this changed so we may need to debug the drop down for the supplier form
    supplier_state = Column(Integer,ForeignKey('states.id'))
    supplier_zip = Column(Integer)
    state= db.relationship('states', foreign_keys=[supplier_state])

class user_groups(db.Model):
    __tablename__="user_groups"
    id = Column(Integer,primary_key=True,autoincrement=True)
    group_desc = Column(String(length=20),unique=True,nullable=False)
    role = Column(String(length=20),unique=True,nullable=False)


class users(db.Model):
    __tablename__ = "users"
    username = Column(String(length=30),unique=True)
    password = Column(String(length=80))
    email = Column(String(length=80),unique=True)
    user_id = Column(Integer,primary_key=True,autoincrement=True)
    sup_dist_admin = Column(Integer,ForeignKey('user_groups.id'))
    sup_id = Column(Integer,ForeignKey('sup_master.id'))
    dist_id = Column(Integer) #todo(aj) ForeignKey
    supplier = db.relationship('sup_master',foreign_keys=[sup_id])


    group = db.relationship('user_groups',foreign_keys=[sup_dist_admin])
    sup = db.relationship('sup_master',foreign_keys=[sup_id])
#    dist = db.relationship('dist_master',foreign_keys=[dist_id])

    @property
    def is_active(self):
        return True

    @property
    def is_authenticated(self):
        return True

    @property
    def is_anonymous(self):
        return False

    def get_id(self):
        try:
            return self.user_id
        except AttributeError:
            raise NotImplementedError('No `id` attribute - override `get_id`')


