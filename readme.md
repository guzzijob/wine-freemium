#setup instructions
1. install dependencies ``` pip install -r requirements.txt ```
1. install postgres database

2. create a database crossmatch_new in postgres

3. set env variables POSTGRES_USER and POSTGRES_PW according to your setup.
export POSTGRES_USER=userame
export POSTGRES_PW = password

4. initialize database changes run ```python migrate.py db init```

5. make database ``` python migrate db upgrade ```

6. run app ``` python app.py ```


#site workflow
1. user signs up using sign in page
2. admin logs in and sets supplier or distributor and the appropriate id
