from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_script import Manager
from app.config import Config
from flask_migrate import Migrate, MigrateCommand

#setup needed objects
app = Flask(__name__)
app.config.from_object(Config)
db = SQLAlchemy(app)

from app.models import *
#setup migrate object
migrate = Migrate(app, db)

#manger
manager = Manager(app)
manager.add_command('db', MigrateCommand)

#import model

if __name__ == '__main__':
    manager.run()