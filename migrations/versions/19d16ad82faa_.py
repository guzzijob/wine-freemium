"""empty message

Revision ID: 19d16ad82faa
Revises: 310fab62829f
Create Date: 2018-04-24 18:26:54.425976

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '19d16ad82faa'
down_revision = '310fab62829f'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_unique_constraint(None, 'user_groups', ['group_desc'])
    op.create_unique_constraint(None, 'user_groups', ['role'])
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint(None, 'user_groups', type_='unique')
    op.drop_constraint(None, 'user_groups', type_='unique')
    # ### end Alembic commands ###
